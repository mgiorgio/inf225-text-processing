package ir.assignments.two.d;

import ir.assignments.two.a.Frequency;
import ir.assignments.two.a.FrequencyComparator;
import ir.assignments.two.a.Utilities;
import ir.assignments.two.c.LiveWordFrequencyCounter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PalindromeFrequencyCounter {
	/**
	 * This class should not be instantiated.
	 */
	private PalindromeFrequencyCounter() {
	}

	/**
	 * Takes the input list of words and processes it, returning a list of
	 * {@link Frequency}s.
	 * 
	 * This method expects a list of lowercase alphanumeric strings. If the
	 * input list is null, an empty list is returned.
	 * 
	 * There is one frequency in the output list for every unique palindrome
	 * found in the original list. The frequency of each palindrome is equal to
	 * the number of times that palindrome occurs in the original list.
	 * 
	 * Palindromes can span sequential words in the input list.
	 * 
	 * The returned list is ordered by decreasing size, with tied palindromes
	 * sorted by frequency and further tied palindromes sorted alphabetically.
	 * 
	 * The original list is not modified.
	 * 
	 * Example:
	 * 
	 * Given the input list of strings ["do", "geese", "see", "god", "abba",
	 * "bat", "tab"]
	 * 
	 * The output list of palindromes should be ["do geese see god:1",
	 * "bat tab:1", "abba:1"]
	 * 
	 * @param words
	 *            A list of words.
	 * @return A list of palindrome frequencies, ordered by decreasing
	 *         frequency.
	 */
	private static List<Frequency> computePalindromeFrequencies(ArrayList<String> words) {
		/*
		 * Although instructions say that palindromes can span multiple words,
		 * it is assumed that if they do, they will span the entire words. For
		 * example, ["abcd", "cbafg"] will NOT produce "abcdcba" palindrome
		 * because "cba" is a subsequence of "cbafg".
		 */
		List<Frequency> frequencies = new ArrayList<Frequency>();

		/*
		 * Let N be the number of words in the list, there can be palindromes of
		 * N different sizes (measured in number of words spanned).
		 */
		for (int i = words.size(); i > 0; i--) {
			frequencies.addAll(getSortedPalindromesOfFixedSize(words, i));
		}

		/*
		 * Since palindromes must be sorted from longer to shorter first, there
		 * is no need to re-sort them since longer palindromes are created first
		 * because of how the for statement is defined. Palindromes with the
		 * same size are already sorted in getSortedPalindromesOfFixedSize().
		 */

		return frequencies;
	}

	/**
	 * Looks for palindromes spanning the given list of words on N terms. For
	 * example, given ["a", "b", "c", "c"] with n=2, this method will verify
	 * whether ["a", "b"], ["b", "c"] and ["c", "c"] are palindromes, being a
	 * palindrome only the last one.
	 * 
	 * @param words
	 *            The {@link List} of words.
	 * @param n
	 *            The spanning length.
	 * @return The {@link List} of {@link Frequency} containing the palindromes.
	 */
	private static List<Frequency> getSortedPalindromesOfFixedSize(ArrayList<String> words, int n) {
		LiveWordFrequencyCounter freqCounter = new LiveWordFrequencyCounter();

		for (int i = 0; i < words.size() - n + 1; i++) {
			MultiWord multiWord = new MultiWord(words.subList(i, n + i));
			if (multiWord.isPalindrome()) {
				freqCounter.increment(multiWord);
			}
		}

		List<Frequency> frequencies = freqCounter.buildListOfFrequencies();

		Collections.sort(frequencies, new FrequencyComparator());

		return frequencies;
	}

	/**
	 * Runs the 2-gram counter. The input should be the path to a text file.
	 * 
	 * @param args
	 *            The first element should contain the path to a text file.
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("Input file must be given as an argument.");
			return;
		}
		File file = new File(args[0]);
		ArrayList<String> words;
		long startTime = System.nanoTime();
		try {
			words = Utilities.tokenizeFile(file);
			List<Frequency> frequencies = computePalindromeFrequencies(words);
			Utilities.printFrequencies(frequencies);

			System.out.println("Elapsed time: " + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime) + " ms.");
		} catch (FileNotFoundException e) {
			System.err.println(args[0] + " does not exist.");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
