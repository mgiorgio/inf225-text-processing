package ir.assignments.two.d;

import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * This class enables iteration over multiple words without the need of joining
 * them. For example, it is possible to recognize palindromes spanning multiple
 * words without allocating more memory.
 * 
 * @author mgiorgio
 * 
 */
public class MultiWord {

	private List<String> words;

	private int hashcode = 0;

	public MultiWord(List<String> words) {
		this.words = words;
	}

	/**
	 * Detects if this multiword is a palindrome.
	 * 
	 * @return <code>true</code> if it is a palindrome. Otherwise,
	 *         <code>false</code>.
	 */
	public boolean isPalindrome() {
		MultiWordIndex index = new MultiWordIndex();
		MultiWordIndex reverseIndex = new MultiWordIndex();

		index.begin(); // Put the cursor at the beginning.
		reverseIndex.end(); // Put the cursor at the end.

		if (index.compareTo(reverseIndex) == 0) {
			// Multiword has length = 1.
			return false;
		}

		while (index.compareTo(reverseIndex) < 0) {
			if (index.charAt() != reverseIndex.charAt()) {
				return false;
			}
			index.forward();
			reverseIndex.rewind();
		}
		return true;
	}

	@Override
	public int hashCode() {
		if(hashcode == 0) {
			this.hashcode = ListUtils.hashCodeForList(words);
		}
		return hashcode;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		return ListUtils.isEqualList(words, ((MultiWord) obj).words);
	}

	@Override
	public String toString() {
		return StringUtils.join(words, " ");
	}

	private class MultiWordIndex {

		private int listIndex;

		private int wordIndex;

		public MultiWordIndex() {
		}

		public char charAt() {
			return words.get(listIndex).charAt(wordIndex);
		}

		public void begin() {
			this.wordIndex = 0;
			this.listIndex = 0;
		}

		public void end() {
			this.listIndex = words.size() - 1;
			this.wordIndex = words.get(listIndex).length() - 1;
		}

		public boolean forward() {
			if (wordIndex < words.get(listIndex).length() - 1) {
				wordIndex++;
				return true;
			} else if (listIndex < words.size() - 1) {
				wordIndex = 0;
				listIndex++;
				return true;
			} else {
				return false;
			}
		}

		public boolean rewind() {
			if (wordIndex > 0) {
				wordIndex--;
				return true;
			} else if (listIndex > 0) {
				listIndex--;
				wordIndex = words.get(listIndex).length() - 1;
				return true;
			} else {
				return false;
			}
		}

		public int compareTo(MultiWordIndex anotherIndex) {
			if (this.listIndex < anotherIndex.listIndex) {
				return -1;
			} else if (this.listIndex > anotherIndex.listIndex) {
				return 1;
			} else {
				if (this.wordIndex == anotherIndex.wordIndex) {
					return 0;
				} else {
					return this.wordIndex - anotherIndex.wordIndex;
				}
			}
		}
	}
}