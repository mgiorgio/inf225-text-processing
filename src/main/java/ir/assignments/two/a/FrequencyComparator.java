package ir.assignments.two.a;

import java.util.Comparator;

/**
 * {@link Comparator} implementation that compares instances of
 * {@link Frequency}. Elements will be sorted by frequency, or text if they tie.
 * 
 * @author mgiorgio
 * 
 */
public class FrequencyComparator implements Comparator<Frequency> {

	public FrequencyComparator() {
	}

	public int compare(Frequency o1, Frequency o2) {
		if (o1.getFrequency() == o2.getFrequency()) {
			return o1.getText().compareTo(o2.getText());
		} else {
			return o2.getFrequency() - o1.getFrequency();
		}
	}

}
