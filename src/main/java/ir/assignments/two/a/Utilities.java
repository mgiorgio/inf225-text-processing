package ir.assignments.two.a;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

/**
 * A collection of utility methods for text processing.
 */
public class Utilities {
	private static final String CONTENT_FIELD = "content";

	/**
	 * Reads the input text file and splits it into alphanumeric tokens. Returns
	 * an ArrayList of these tokens, ordered according to their occurrence in
	 * the original text file.
	 * 
	 * Non-alphanumeric characters delineate tokens, and are discarded.
	 * 
	 * Words are also normalized to lower case.
	 * 
	 * Example:
	 * 
	 * Given this input string "An input string, this is! (or is it?)"
	 * 
	 * The output list of strings should be ["an", "input", "string", "this",
	 * "is", "or", "is", "it"]
	 * 
	 * @param input
	 *            The file to read in and tokenize.
	 * @return The list of tokens (words) from the input file, ordered by
	 *         occurrence.
	 * @throws IOException
	 *             If the given file cannot be accessed or if something
	 *             unexpected occurs during the tokenization.
	 * @throws FileNotFoundException
	 *             If the given file does not exist.
	 */
	public static ArrayList<String> tokenizeFile(File input) throws FileNotFoundException, IOException {
		ArrayList<String> listOfWords = new ArrayList<String>();
		Analyzer analyzer = createLuceneAnalyzer();
		TokenStream ts;
		ts = analyzer.tokenStream(CONTENT_FIELD, new FileReader(input));

		CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);

		try {
			ts.reset(); // Resets this stream to the beginning.
			while (ts.incrementToken()) {
				listOfWords.add(termAtt.toString());
			}
			ts.end(); // Perform end-of-stream operations, e.g. set
						// the
						// final
						// offset.
		} finally {
			ts.close(); // Release resources associated with this
						// stream.
		}

		return listOfWords;
	}

	private static Analyzer createLuceneAnalyzer() {
		return new SimpleAnalyzer();
	}

	/**
	 * Takes a list of {@link Frequency}s and prints it to standard out. It also
	 * prints out the total number of items, and the total number of unique
	 * items.
	 * 
	 * Example one:
	 * 
	 * Given the input list of word frequencies ["sentence:2", "the:1",
	 * "this:1", "repeats:1", "word:1"]
	 * 
	 * The following should be printed to standard out
	 * 
	 * Total item count: 6 Unique item count: 5
	 * 
	 * sentence 2 the 1 this 1 repeats 1 word 1
	 * 
	 * 
	 * Example two:
	 * 
	 * Given the input list of 2-gram frequencies ["you think:2", "how you:1",
	 * "know how:1", "think you:1", "you know:1"]
	 * 
	 * The following should be printed to standard out
	 * 
	 * Total 2-gram count: 6 Unique 2-gram count: 5
	 * 
	 * you think 2 how you 1 know how 1 think you 1 you know 1
	 * 
	 * @param frequencies
	 *            A list of frequencies.
	 */
	public static void printFrequencies(List<Frequency> frequencies) {
		if (frequencies.isEmpty()) {
			System.out.println("Total item count: 0 Unique item count: 0");
			return;
		}
		/*
		 * Print header.
		 */
		System.out.println(createPrintHeader(frequencies));

		// Print all frequencies. Since it is not specified in the requirements,
		// the List will NOT be sorted before print it.
		for (Frequency frequency : frequencies) {
			System.out.print(formatFrequencyString(frequency));
		}
		System.out.println();
	}

	/**
	 * Formats a single {@link Frequency} to be printed.
	 * 
	 * @param frequency
	 *            The {@link Frequency} to be formatted.
	 * @return A {@link String} ready to be printed.
	 */
	private static String formatFrequencyString(Frequency frequency) {
		StringBuilder builder = new StringBuilder();
		builder.append(frequency.getText()).append(" ").append(frequency.getFrequency()).append(" ");
		return builder.toString();
	}

	/**
	 * Creates the frequencies header for printing including the total number of
	 * terms and the unique count.
	 * 
	 * @param frequencies
	 *            The {@link List} of {@link Frequency}.
	 * @return A String ready to be printed.
	 */
	private static String createPrintHeader(List<Frequency> frequencies) {
		String termDenomination = determineNgramDenomination(frequencies);

		long totalNumberOfItems = countTotalNumberOfItems(frequencies);

		StringBuilder builder = new StringBuilder();

		builder.append("Total ").append(termDenomination).append(" count: ").append(totalNumberOfItems);
		builder.append(" Unique ").append(termDenomination).append(" count: ").append(frequencies.size());
		return builder.toString();
	}

	/**
	 * Counts the total number of terms present in the {@link List} of
	 * {@link Frequency}. It is the sum of all of the frequencies.
	 * 
	 * @param frequencies
	 *            The {@link List} of {@link Frequency}.
	 * @return The total number of items.
	 */
	private static long countTotalNumberOfItems(List<Frequency> frequencies) {
		long totalNumberOfItems = 0L;
		for (Frequency frequency : frequencies) {
			totalNumberOfItems += frequency.getFrequency();
		}
		return totalNumberOfItems;
	}

	/**
	 * Since the requirements specify that 1-gram terms must be printed as
	 * "item" and N-gram terms with N>1 must be printed as "N-gram", this
	 * methods returns the denomination that has to be used for a given
	 * {@link List} of {@link Frequency}.
	 * 
	 * @param frequencies
	 *            A {@link List} of {@link Frequency}.
	 * @return The denomination to be used for these terms.
	 */
	private static String determineNgramDenomination(List<Frequency> frequencies) {
		int n = calculateNinNgram(frequencies); // n will be 1 for 1-gram, 2 for
												// 2-gram and so on.
		return (n == 1) ? "item" : n + "-gram";
	}

	/**
	 * Determines if the terms contained in the given {@link List} is 1-gram,
	 * 2-gram, etc. This method assumes that all of the terms in the
	 * {@link List} are the same N-gram.
	 * 
	 * @param frequencies
	 *            A {@link List} of {@link Frequency}.
	 * @return 1 if the terms are 1-gram, 2 if the terms are 2-gram and so on.
	 */
	private static int calculateNinNgram(List<Frequency> frequencies) {
		int maxn = 0;
		for (Frequency frequency : frequencies) {
			int currentn = StringUtils.countMatches(frequency.getText(), " ") + 1;

			if (maxn == 0) {
				maxn = currentn;
			} else if (maxn != currentn) {
				return 1;
			}
		}
		return maxn;
	}
}