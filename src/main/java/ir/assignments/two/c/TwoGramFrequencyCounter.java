package ir.assignments.two.c;

import ir.assignments.two.a.Frequency;
import ir.assignments.two.a.FrequencyComparator;
import ir.assignments.two.a.Utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Count the total number of 2-grams and their frequencies in a text file.
 */
public final class TwoGramFrequencyCounter {
	/**
	 * This class should not be instantiated.
	 */
	private TwoGramFrequencyCounter() {
	}

	/**
	 * Takes the input list of words and processes it, returning a list of
	 * {@link Frequency}s.
	 * 
	 * This method expects a list of lowercase alphanumeric strings. If the
	 * input list is null, an empty list is returned.
	 * 
	 * There is one frequency in the output list for every unique 2-gram in the
	 * original list. The frequency of each 2-grams is equal to the number of
	 * times that two-gram occurs in the original list.
	 * 
	 * The returned list is ordered by decreasing frequency, with tied 2-grams
	 * sorted alphabetically.
	 * 
	 * 
	 * 
	 * Example:
	 * 
	 * Given the input list of strings ["you", "think", "you", "know", "how",
	 * "you", "think"]
	 * 
	 * The output list of 2-gram frequencies should be ["you think:2",
	 * "how you:1", "know how:1", "think you:1", "you know:1"]
	 * 
	 * @param words
	 *            A list of words.
	 * @return A list of two gram frequencies, ordered by decreasing frequency.
	 */
	private static List<Frequency> computeTwoGramFrequencies(ArrayList<String> words) {
		if (words.isEmpty()) {
			return new ArrayList<Frequency>(1);
		}
		LiveWordFrequencyCounter freqCounter = new LiveWordFrequencyCounter();

		for (int i = 0; i < words.size() - 1; i++) {
			// Get the two single terms that will create the 2-gram term. We
			// assume that the terms contained in the given list are 1-gram
			// terms.
			String term1 = words.get(i);
			String term2 = words.get(i + 1);

			// Length of both plus the whitespace.
			StringBuilder builder = new StringBuilder(term1.length() + term2.length() + 1);
			// Concatenate both terms putting a whitespace in the middle.
			builder.append(term1).append(" ").append(term2);
			String the2gramTerm = builder.toString();

			freqCounter.increment(the2gramTerm);
		}

		List<Frequency> listOfFrequencies = freqCounter.buildListOfFrequencies();

		Collections.sort(listOfFrequencies, new FrequencyComparator());

		return listOfFrequencies;
	}

	/**
	 * Runs the 2-gram counter. The input should be the path to a text file.
	 * 
	 * @param args
	 *            The first element should contain the path to a text file.
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("Input file must be given as an argument.");
			return;
		}
		File file = new File(args[0]);
		ArrayList<String> words;
		long startTime = System.nanoTime();
		try {
			words = Utilities.tokenizeFile(file);
			List<Frequency> frequencies = computeTwoGramFrequencies(words);
			Utilities.printFrequencies(frequencies);
			System.out.println("Elapsed time: " + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime) + " ms.");
		} catch (FileNotFoundException e) {
			System.err.println(args[0] + " does not exist.");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
