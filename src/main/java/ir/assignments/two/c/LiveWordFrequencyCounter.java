package ir.assignments.two.c;

import ir.assignments.two.a.Frequency;
import ir.assignments.two.b.WordFrequencyCounter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * This class keeps track of the frequency of the terms that it receives in the
 * method {@link LiveWordFrequencyCounter#increment(String)}
 * </p>
 * <p>
 * The only difference with {@link WordFrequencyCounter} is that it requires a
 * {@link List} of terms already created whereas
 * {@link LiveWordFrequencyCounter} can add terms as they appear.
 * </p>
 * 
 * @author mgiorgio
 * 
 */
public class LiveWordFrequencyCounter {

	private Map<Object, Integer> cardinalityMap = new HashMap<Object, Integer>();

	public LiveWordFrequencyCounter() {
	}

	public void increment(Object term) {
		// Get the current frequency.
		Integer currentFrequency = cardinalityMap.get(term);

		// Update frequency.
		if (currentFrequency == null) {
			currentFrequency = 1;
		} else {
			currentFrequency += 1;
		}
		// Set the new frequency.
		cardinalityMap.put(term, currentFrequency);
	}

	public List<Frequency> buildListOfFrequencies() {
		return WordFrequencyCounter.convertCardinalityMapToListOfFrequencies(cardinalityMap);
	}

}
