package ir.assignments.two.b;

import ir.assignments.two.a.Frequency;
import ir.assignments.two.a.FrequencyComparator;
import ir.assignments.two.a.Utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;

/**
 * Counts the total number of words and their frequencies in a text file.
 */
public final class WordFrequencyCounter {
	/**
	 * This class should not be instantiated.
	 */
	private WordFrequencyCounter() {
	}

	/**
	 * Takes the input list of words and processes it, returning a list of
	 * {@link Frequency}s.
	 * 
	 * This method expects a list of lowercase alphanumeric strings. If the
	 * input list is null, an empty list is returned.
	 * 
	 * There is one frequency in the output list for every unique word in the
	 * original list. The frequency of each word is equal to the number of times
	 * that word occurs in the original list.
	 * 
	 * The returned list is ordered by decreasing frequency, with tied words
	 * sorted alphabetically.
	 * 
	 * The original list is not modified.
	 * 
	 * Example:
	 * 
	 * Given the input list of strings ["this", "sentence", "repeats", "the",
	 * "word", "sentence"]
	 * 
	 * The output list of frequencies should be ["sentence:2", "the:1",
	 * "this:1", "repeats:1", "word:1"]
	 * 
	 * @param words
	 *            A list of words.
	 * @return A list of word frequencies, ordered by decreasing frequency.
	 */
	public static List<Frequency> computeWordFrequencies(List<String> words) {
		@SuppressWarnings("unchecked")
		Map<Object, Integer> cardinalityMap = CollectionUtils.getCardinalityMap(words);

		List<Frequency> frequencies = convertCardinalityMapToListOfFrequencies(cardinalityMap);

		Collections.sort(frequencies, new FrequencyComparator());

		return frequencies;
	}

	public static List<Frequency> convertCardinalityMapToListOfFrequencies(Map<Object, Integer> cardinalityMap) {
		List<Frequency> frequencies = new ArrayList<Frequency>(cardinalityMap.size());

		for (Entry<Object, Integer> entry : cardinalityMap.entrySet()) {
			frequencies.add(new Frequency(entry.getKey().toString(), entry.getValue()));
		}
		return frequencies;
	}

	/**
	 * Runs the word frequency counter. The input should be the path to a text
	 * file.
	 * 
	 * @param args
	 *            The first element should contain the path to a text file.
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("Input file must be given as an argument.");
			return;
		}
		File file = new File(args[0]);
		List<String> words;
		long startTime = System.nanoTime();
		try {
			words = Utilities.tokenizeFile(file);
			List<Frequency> frequencies = computeWordFrequencies(words);
			Utilities.printFrequencies(frequencies);
			System.out.println("Elapsed time: " + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime) + " ms.");
		} catch (FileNotFoundException e) {
			System.err.println(args[0] + " does not exist.");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
