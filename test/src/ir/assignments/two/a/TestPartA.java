package ir.assignments.two.a;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class TestPartA {

	@Test
	public void visuallyAssertPrintingFrequencies() {
		print1gramFrequencies();
		print2gramFrequencies();
	}

	private void print1gramFrequencies() {
		Frequency freq1 = createFrequency("horse", 1);
		Frequency freq2 = createFrequency("dog", 2);
		Frequency freq3 = createFrequency("cat", 0);
		Frequency freq4 = createFrequency("bird", 4);

		Utilities.printFrequencies(Arrays.asList(freq1, freq2, freq3, freq4));
	}

	private void print2gramFrequencies() {
		Frequency freq1 = createFrequency("black horse", 1);
		Frequency freq2 = createFrequency("gray dog", 2);
		Frequency freq3 = createFrequency("white cat", 0);
		Frequency freq4 = createFrequency("red bird", 4);

		Utilities.printFrequencies(Arrays.asList(freq1, freq2, freq3, freq4));
	}

	private Frequency createFrequency(String word, int frequency) {
		return new Frequency(word, frequency);
	}

	@Test
	public void assertTokenizationInInputFiles() throws IOException {
		assertTokenization("resources/input01.txt", "resources/input01-tokens.txt");
	}

	private void assertTokenization(String filename, String tokensFilename) throws FileNotFoundException, IOException {
		File file = new File(filename);
		File tokensFile = new File(tokensFilename);

		List<String> expectedTokens = Arrays.asList(StringUtils.split(FileUtils.readFileToString(tokensFile)));
		ArrayList<String> observedTokens = Utilities.tokenizeFile(file);

		Assert.assertTrue("Tokenized file is different from expected result.", CollectionUtils.isEqualCollection(observedTokens, expectedTokens));
	}
}